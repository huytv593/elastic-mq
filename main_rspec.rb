require_relative 'main'

describe "Main" do
  before do
    @main = Main.new() ## initialize Multiple object
  end

  describe "should create queue sucessful" do
    it "normal queue" do
      result = @main.create_queue
      expect(result).to eql("awesome_queue")
    end
  end
end